# Les bonnes pratiques dans votre recherche de stage
## David Cossé - Noëmie Dagusé

---
# À quel moment rechercher son entreprise d'accueil ?

- S’y prendre le plus tôt possible (dès que l’on connaît la période de stage)
- Idéal: 4 mois

> Rechercher un stage prend environ 4 mois.
> Compte tenue de la crise, la recherche peut prendre plus de temps.
> Dès que vous connaissez vos périodes de stage, il faut commencer à postuler.
> Ne pas oublier que vous ne serez pas les seuls à candidater


---

# Où et comment rechercher ?

- Réseaux d’entreprise
- LinkedIn
- Votre réseau

---

# Réseaux d'entreprises

Ils sont souvent organisés selon:
- Des centres d’intérêt techno
- Des centres d’intérêt philosophique
- Des secteurs d’activité


> Dans certaines villes / départements / régions... il y a des réseaux d'entreprises en fonction : 
> - des centres d'intérêt techno (ex : PGGTIE pour PostgreSQL, le JUG pour Java, GDG pour google ...);
> - des centres d'intérêt philosophique (ex : l'APRIL, Alliance Libre, Libre entreprise...);
> - des secteurs d'activité (ADN'Ouest pour Nantes et sa région ...).
> - Sur chacun des sites internet de ces réseaux,
> il y a une page dédiée aux membres.
> Cela peut vous donner une bonne base pour aller chercher les entreprises dans lesquelles postuler.

---

# LinkedIn

- Se créer un profil
- Se mettre en relation
- Faire des recherches
- Chercher les propositions de stages

> - Créez-vous un profil, et commencez à établir votre réseau.
> - En tapant des mots clés vous pourrez trouver des entreprises et des personnes.
> - Si vous cherchez des sociétés faisant du Java par exemple, vous pouvez taper le mot "Java" et une ville de votre choix, vous trouverez de nombreuses personnes faisant du java dans la ville en question.
> - De là vous pouvez, soit contacter l'entreprise par téléphone ou mail en allant sur leur site internet, soit contacter directement la personne via linkedIn en lui envoyant un message.
> - Des entreprises proposent des stages via linkedIn... il est important d'aller y faire un tour de temps en temps.

---

# Votre réseau

- Jobs d'été
- Participations à des conférences
- Parents / famille
- Professeurs

> Créez votre propre réseau:
> - Retenez le nom des contacts intéressants que vous pouvez avoir lors de:
>   - vos jobs d'été,
>   - vos participations à des conférences (JUG, DEVOXX, FOSDEM, DEVFEST, RRLL... ) vous faites des rencontres, linkedIn vous permet de garder un lien avec ces personnes.
>   - vos premiers contacts seront très probablement des proches (parents, oncles, tantes, amis de parents...).
> - Il ne faut pas hésiter à entrer en contact avec des personnes.

---

# Le suivi d'une candidature

## Tableau de suivi, que mettre dedans?
- Nom de l’entreprise, adresse, numéro de téléphone, coordonnées du contact
- Technologies
- Date d’envoi, date de réponse, date de relance
- Commentaires

> Ce tableau est à mettre à jour à chaque réponse
> - Renseigner les refus
> - Renseigner les informations collectées.
> - Les réponses même négatives sont à indiquer dans le tableau, cela évitera de relancer 2 fois la même entreprise. Cette année la réponse a été négative, mais elle sera peut-être positive une autre année.
> - Les indications que vous donnent les entreprises sont également à indiquer en "commentaires" (contact agréable, travail sur tel ou tel projet, taille de l'entreprise...).
>
> Conservez ce tableau, il vous sera utile tout au long de votre formation et très probablement au début de votre carrière!

---
# La Lettre de motivation

## Comment organiser la lettre de motivation?
- La société
- Vous
- Vous et la société
- Formule de politesse

> - C'est le premier regard qu'à l'entreprise sur vous.
> - Elle peut être écrite à l'intérieur d'un corps de mail ou en pièce jointe mais dans ce cas dans un format type PDF, pas d'odt, .doc ou docx.
> - Vous y décrivez le pourquoi de cette candidature,
> - les raisons qui vous amènent à postuler dans cette entreprise,
> - ce qui vous attire dans cette candidature dans l'offre de stage (si réponse à une offre)
> - Vous expliquez qui vous êtes sans répéter le CV, vous pouvez par exemple expliquer une situation en entreprise ou dans une asso qui explique un trait de votre personnalité
> - Vous indiquez ce que la société pourra vous apporter et ce que vous vous engagez à faire

---

# Le ton de la lettre
- Professionnel
- Votre image
- Votre motivation

> - Elle doit être professionnelle
> - Bien écrite (pas de répétition, un vocabulaire varié), pas de fautes https://grammalecte.net/ ! Faites-vous relire... Nous ne lisons plus les lettres ayant plus de 2 fautes.
> - À votre image.
> - Elle reflète ce que vous êtes.
> - C’est notre premier aperçu de vous.

---
# Le CV

## Que mettre dans un CV ?
- Les technologies étudiées
- Les expériences
- Les projets personnels
- Les hobbies
- Les soft skills


> C'est votre carte d'identité professionnelle
> Indiquez vos expériences acquises lors des précédents stages.
> Indiquez vos expériences acquises lors de vos jobs d'été.

> Les Soft skill: ce sont les compétences comportementales et humaines :
> - curiosité,
> - empathie,
> - communication,
> - la gestion du stress,
> - la créativité,
> - la résolution de problèmes,
> - l'audace.
> Les Soft skills doivent être corrélées avec des expériences indiquées sur le CV ou la lettre de motivation.

> En fin de CV ajoutez quelques mots sur vos passions.

> Ne prenez pas de CV trouvé sur le web, tout fait. Personnalisez-le!
> Le CV est une image de vous même : il est important d'être sincère mais de le soigner.

> Le format peut-être PDF ou web

> Il doit être clair et bien organisé. Il présente vos compétences tant d'un point de vue techno que d'un point de vue "soft skills.

> Pour présenter les langages que vous connaissez il semble présomptueux de donner une note à une technologie: n'utilisez pas d'histogramme (présentant votre niveau de connaissance de 1 à 5) mais privilégiez une approche avec un graphique type camembert dans lequel vous proportionnez la part d'un langage en rapport avec votre appétit pour celui-ci.

# La relance

- Espacer de **7 jours** entre l'envoi et la relance
- Répondre aux mails même si la réponse est négative
- Laisser **une bonne impression**

# L’entretien

Quelques principes de base : 
 - Arriver à l’heure
 - Tenue propre et professionnelle
 - Posture convenable, sourire
 - Prendre son CV

> Soyez acteur de votre entretient
> - intervenez
> - posez des questions (préparez une liste de questions)
> - tenue : jean, chemise, pull (pas de sweat à capuche)
> - entraînez-vous à vous présenter (avec vos professeurs, vos parents, devant un miroir...)

# L’entretien

Se préparer à l’entretien
- Répéter sa présentation
- Préparer les questions récurrentes
- Arriver avec des questions

# Les questions récurrentes

- Peux-tu te présenter ?
- 3 qualités / défauts
- Pourquoi as-tu postulé dans notre entreprise ?
- Que dit ta famille de toi ? 
- Où te vois-tu l’année prochaine ?
- Comptes-tu continuer tes études ?
- Tests techniques ...

# Lors du stage

- Attitude professionnelle
- Créer du lien social
- Être force de proposition
- Accepter les remarques

> On attend de vous une attitude professionnelle
>  - du sérieux, cela n'empêche pas les traits d'humour
>  - de la rigueur, ne passez votre temps sur les réseaux sociaux ou votre portable...
>  - de l'implication, apportez du soin, donnez-vous les moyens d'atteindre les objectifs du stage
>  - de la politesse (dites bonjour, à tous le monde si l'entreprise n'est pas très grande)
>  - de l'interaction, participez aux déjeuners
>  - lorsque vous arrivez au sein d'une entreprise, d'une équipe vous faites partie de l'équipe et l'on attend de vous que vous y participiez non pas de façon attentiste mais active.
> Vous serez peut-être amenés à être mis en situation comme si vous présentiez votre travail à un client.
> Vous aurez certainement des remarques sur votre travail et/ou votre méthodologie tenez en compte!
> N'oubliez pas que le stage est parfois la porte d'entrée vers un emploi!
